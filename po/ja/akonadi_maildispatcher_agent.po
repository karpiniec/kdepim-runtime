# Fumiaki Okushi <okushi@kde.gr.jp>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: akonadi_maildispatcher_agent\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-04 00:48+0000\n"
"PO-Revision-Date: 2010-11-21 13:47-0800\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: maildispatcheragent.cpp:84 maildispatcheragent.cpp:233
#, kde-format
msgid "Sending messages (1 item in queue)..."
msgid_plural "Sending messages (%1 items in queue)..."
msgstr[0] "メッセージを送信中 (残り 1 通)"
msgstr[1] "メッセージを送信中 (残り %1 通)"

#: maildispatcheragent.cpp:94
#, kde-format
msgid "Sending canceled."
msgstr "送信をキャンセルしました。"

#: maildispatcheragent.cpp:101
#, kde-format
msgid "Finished sending messages."
msgstr "メッセージの送信を終了しました。"

#: maildispatcheragent.cpp:107
#, fuzzy, kde-format
#| msgctxt "Notification when the email was sent"
#| msgid "E-mail successfully sent"
msgctxt "Notification title when email was sent"
msgid "E-Mail Successfully Sent"
msgstr "メールを送信しました"

#: maildispatcheragent.cpp:108
#, fuzzy, kde-format
#| msgctxt "Notification when the email was sent"
#| msgid "E-mail successfully sent"
msgctxt "Notification when the email was sent"
msgid "Your E-Mail has been sent successfully."
msgstr "メールを送信しました"

#: maildispatcheragent.cpp:114
#, kde-format
msgid "No items in queue."
msgstr ""

#: maildispatcheragent.cpp:164
#, kde-format
msgid "Online, sending messages in queue."
msgstr "オンライン、待ち行列のメッセージを送信中。"

#: maildispatcheragent.cpp:168
#, kde-format
msgid "Offline, message sending suspended."
msgstr "オフライン、メッセージの送信を一時中止。"

#: maildispatcheragent.cpp:193
#, kde-format
msgctxt "Message with given subject is being sent."
msgid "Sending: %1"
msgstr "送信中: %1"

#: maildispatcheragent.cpp:258
#, fuzzy, kde-format
#| msgid "Sending canceled."
msgctxt "Notification title when email sending failed"
msgid "E-Mail Sending Failed"
msgstr "送信をキャンセルしました。"

#: maildispatcheragent.cpp:291
#, kde-format
msgid "Ready to dispatch messages."
msgstr ""

#. i18n: ectx: label, entry (Outbox), group (General)
#: maildispatcheragent.kcfg:10
#, kde-format
msgid "Outbox collection id"
msgstr ""

#. i18n: ectx: label, entry (SentMail), group (General)
#: maildispatcheragent.kcfg:14
#, kde-format
msgid "Sent Mail collection id"
msgstr ""

#: outboxqueue.cpp:265
#, kde-format
msgid "Could not access the outbox folder (%1)."
msgstr "送信待ちフォルダ (%1) をアクセスできませんでした。"

#: sendjob.cpp:49 sendjob.cpp:50
#, kde-format
msgid "Message sending aborted."
msgstr "メッセージの送信を中止しました。"

#: sendjob.cpp:59
#, kde-format
msgid "Could not initiate message transport. Possibly invalid transport."
msgstr ""

#: sendjob.cpp:65
#, kde-format
msgid "Could not send message. Invalid transport."
msgstr ""

#: sendjob.cpp:97
#, kde-format
msgid "Failed to get D-Bus interface of resource %1."
msgstr ""

#: sendjob.cpp:110
#, kde-format
msgid "Invalid D-Bus reply from resource %1."
msgstr ""

#: sendjob.cpp:228
#, fuzzy, kde-format
#| msgid "Invalid sent-mail folder. Keeping message in outbox."
msgid "Default sent-mail folder unavailable. Keeping message in outbox."
msgstr "送信済みフォルダが不正です。メッセージを送信待ちフォルダに残します。"

#: sendjob.cpp:240
#, kde-format
msgid "Message transport aborted."
msgstr ""

#: sendjob.cpp:240
#, kde-format
msgid "Failed to transport message."
msgstr ""

#: sendjob.cpp:282
#, kde-format
msgid "Failed to get D-Bus interface of mailfilteragent."
msgstr ""

#: sendjob.cpp:289
#, kde-format
msgid "Invalid D-Bus reply from mailfilteragent"
msgstr ""

#: sendjob.cpp:331
#, kde-format
msgid "Sending succeeded, but failed to remove the message from the outbox."
msgstr ""

#: sendjob.cpp:334
#, kde-format
msgid ""
"Sending succeeded, but failed to move the message to the sent-mail folder."
msgstr ""

#: sendjob.cpp:370
#, kde-format
msgid "Failed to store result in item."
msgstr ""

#: storeresultjob.cpp:59
#, kde-format
msgid "Failed to fetch item."
msgstr ""
