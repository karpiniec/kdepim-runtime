msgid ""
msgstr ""
"Project-Id-Version: accountwizard_vcarddir\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2014-04-02 18:56+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: vcarddirwizard.es.cmake:7
msgid "Settings"
msgstr "Configuração"

#: vcarddirwizard.es.cmake:24
msgid "Default Contact"
msgstr "Contacto Predefinido"

#. i18n: ectx: property (text), widget (QLabel, label)
#: vcarddirwizard.ui:19
#, kde-format
msgid "Path:"
msgstr "Localização:"
