# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2016, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-25 00:29+0000\n"
"PO-Revision-Date: 2017-12-11 19:45+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: tomboyitemuploadjob.cpp:107 tomboyitemuploadjob.cpp:113
#, kde-format
msgid "Sync error. Server status not as expected."
msgstr "Synkronointivirhe. Palvelimen tila ei ollut odotettu."

#. i18n: ectx: property (windowTitle), widget (QWidget, TomboyNotesAgentConfigWidget)
#: tomboynotesagentconfigwidget.ui:14
#, kde-format
msgid "Tomboy Server Settings"
msgstr "Tomboy-palvelinasetukset"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: tomboynotesagentconfigwidget.ui:36
#, kde-format
msgid "Tomboy"
msgstr "Tomboy"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: tomboynotesagentconfigwidget.ui:42
#, kde-format
msgid "Display name:"
msgstr "Näyttönimi:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: tomboynotesagentconfigwidget.ui:56
#, kde-format
msgid "Type in the server URL:"
msgstr "Anna palvelimen verkko-osoite:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, kcfg_ServerURL)
#: tomboynotesagentconfigwidget.ui:63
#, kde-format
msgid "This value is not changeable after first setup"
msgstr "Arvoa ei voi vaihtaa ensiasetuksen jälkeen"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: tomboynotesagentconfigwidget.ui:76
#, kde-format
msgid "Update interval:"
msgstr "Päivitysväli:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: tomboynotesagentconfigwidget.ui:86
#, kde-format
msgid "minutes"
msgstr "min"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ReadOnly)
#: tomboynotesagentconfigwidget.ui:96
#, kde-format
msgid "Open in read-only mode"
msgstr "Avaa vain luku -tilassa"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ignoreSslErrors)
#: tomboynotesagentconfigwidget.ui:103
#, kde-format
msgid "Ignore SSL errors"
msgstr "Ohita SSL-virheet"

#: tomboynotesconfigwidget.cpp:48
#, kde-format
msgid "Tomboy Notes %1"
msgstr "Tomboy-muistiinpanot %1"

#: tomboynotesresource.cpp:58 tomboynotesresource.cpp:79
#: tomboynotesresource.cpp:98
#, kde-format
msgid "Resource configuration is not valid"
msgstr "Resurssin asetus ei ole kelvollinen"

#: tomboynotesresource.cpp:224 tomboynotesresource.cpp:245
#: tomboynotesresource.cpp:265
#, kde-format
msgid "Resource is read-only"
msgstr "Resurssi on vain luettavissa"

#. i18n: ectx: label, entry (collectionName), group (General)
#: tomboynotesresource.kcfg:10
#, kde-format
msgid "The display name of this resource"
msgstr "Tämän resurssin näyttönimi"

#. i18n: ectx: label, entry (ServerURL), group (General)
#: tomboynotesresource.kcfg:14
#, kde-format
msgid "The URL to the Tomboy sync server"
msgstr "Tomboy-synkronointipalvelimen verkko-osoite"

#. i18n: ectx: label, entry (contentURL), group (General)
#: tomboynotesresource.kcfg:18
#, kde-format
msgid "The URL to the notes on the Tomboy sync server"
msgstr "Muistiinpanojen verkko-osoite Tomboy-synkronointipalvelimessa"

#. i18n: ectx: label, entry (userURL), group (General)
#: tomboynotesresource.kcfg:22
#, kde-format
msgid "The URL to the user data on the Tomboy sync server"
msgstr "Käyttäjän tietojen verkko-osoite Tomboy-synkronointipalvelimessa"

#. i18n: ectx: label, entry (requestToken), group (General)
#: tomboynotesresource.kcfg:26
#, kde-format
msgid "The request token to the Tomboy sync server"
msgstr "Tomboy-synkronointipalvelimen pyyntötunniste"

# Varmaankaan kyse ei ole salasanasta sinänsä, mutta ”salaisuus” (salaisuudet) tuntuu jotenkin lapselliselta :)
#. i18n: ectx: label, entry (requestTokenSecret), group (General)
#: tomboynotesresource.kcfg:30
#, kde-format
msgid "The request token secret to the Tomboy sync server"
msgstr "Tomboy-synkronointipalvelimen pyyntötunnisteen salasana"

#. i18n: ectx: label, entry (ConflictHandling), group (General)
#: tomboynotesresource.kcfg:34
#, kde-format
msgid "The way how conflicts should be handled"
msgstr "Ristiriitojen käsittelytapa"

#. i18n: ectx: label, entry (ReadOnly), group (General)
#: tomboynotesresource.kcfg:38
#, kde-format
msgid "Do not change the actual backend data."
msgstr "Älä muuta tietoja varsinaisessa taustaosassa."

#. i18n: ectx: label, entry (ignoreSslErrors), group (General)
#: tomboynotesresource.kcfg:42
#, kde-format
msgid "Ignore SSL errors."
msgstr "Ohita SSL-virheet."

#. i18n: ectx: label, entry (refreshInterval), group (General)
#: tomboynotesresource.kcfg:46
#, kde-format
msgid "Refresh every"
msgstr "Päivitä joka"

#: tomboyserverauthenticatejob.cpp:58
#, kde-format
msgid "Authorization failed. It could be an SSL error."
msgstr "Varmennus epäonnistui. Virhe saattaa johtua SSL:stä."

#~ msgid "Select a Tomboy server"
#~ msgstr "Valitse Tomboy-palvelin"
