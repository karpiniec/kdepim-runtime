# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Xosé <xosecalvo@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2014-04-25 23:23+0200\n"
"Last-Translator: Xosé <xosecalvo@gmail.com>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contactswizard.es.cmake:7
msgid "Settings"
msgstr "Configuración"

#: contactswizard.es.cmake:25
msgid "Local Contacts"
msgstr "Contactos locais"

#. i18n: ectx: property (text), widget (QLabel, label)
#: contactswizard.ui:19
#, kde-format
msgid "Filename:"
msgstr "Nome do ficheiro:"
