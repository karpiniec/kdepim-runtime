# translation of newmailnotifieragent.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2011.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: newmailnotifieragent\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-26 00:48+0000\n"
"PO-Revision-Date: 2014-08-13 00:18+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: newmailnotifieragent.cpp:339
#, fuzzy, kde-format
#| msgid "One new email in %2 from \"%3\""
#| msgid_plural "%1 new emails in %2 from \"%3\""
msgctxt ""
"%2 = name of mail folder; %3 = name of Akonadi POP3/IMAP/etc resource (as "
"user named it)"
msgid "One new email in %2 from \"%3\""
msgid_plural "%1 new emails in %2 from \"%3\""
msgstr[0] "Een nieg Nettbreef binnen %2 vun \"%3\""
msgstr[1] "%1 nieg Nettbreven binnen %2 vun \"%3\""

#: newmailnotifieragent.cpp:359
#, kde-format
msgid "New mail arrived"
msgstr "Nieg Nettpost ankamen"

#: newmailnotifieragentsettings.kcfg:34
#, fuzzy, kde-format
#| msgctxt "%s is a variable for agent. Do not change it"
#| msgid "A message was received from %s"
msgctxt "%f is a variable for agent. Do not change it"
msgid "A message was received from %f"
msgstr "En Naricht vun %s is ankamen"

#: newmailnotifierreplymessagejob.cpp:38 newmailnotifiershowmessagejob.cpp:36
#, kde-format
msgid "Unable to start KMail application."
msgstr ""

#: newmailnotifierselectcollectionwidget.cpp:94
#, kde-format
msgid "Select which folders to monitor for new message notifications:"
msgstr "De Ornern utsöken, de Du op Nieg-Naricht-Bescheden beluern wullt:"

#: newmailnotifierselectcollectionwidget.cpp:125
#, kde-format
msgid "Search..."
msgstr "Söken..."

#: newmailnotifierselectcollectionwidget.cpp:140
#, kde-format
msgid "&Select All"
msgstr "&All utsöken"

#: newmailnotifierselectcollectionwidget.cpp:144
#, kde-format
msgid "&Unselect All"
msgstr "Köör &torüchnehmen"

#: newmailnotifiersettingswidget.cpp:31
#, c-format
msgid ""
"<qt><p>Here you can define message. You can use:</p><ul><li>%s set subject</"
"li><li>%f set from</li></ul></qt>"
msgstr ""
"<qt> <p>Hier kannst Du de Naricht fastleggen. Du kannst disse Platzhollers "
"bruken:</p> <ul> <li>%s geven Bedraap</li> <li>%f geven Afsenner</li> </ul> "
"</qt>"

#: newmailnotifiersettingswidget.cpp:53
#, kde-format
msgid "Choose which fields to show:"
msgstr "De Feller utsöken, de Du wiesen wullt:"

#: newmailnotifiersettingswidget.cpp:58
#, kde-format
msgid "Show Photo"
msgstr "Foto wiesen"

#: newmailnotifiersettingswidget.cpp:62
#, kde-format
msgid "Show From"
msgstr "Vun wiesen"

#: newmailnotifiersettingswidget.cpp:66
#, kde-format
msgid "Show Subject"
msgstr "Bedraap wiesen"

#: newmailnotifiersettingswidget.cpp:70
#, kde-format
msgid "Show Folders"
msgstr "Ornern wiesen"

#: newmailnotifiersettingswidget.cpp:74
#, kde-format
msgid "Do not notify when email was sent by me"
msgstr "Nich Bescheed geven, wenn ik den Nettbreef loosstüert heff"

#: newmailnotifiersettingswidget.cpp:78
#, kde-format
msgid "Keep Persistent Notification"
msgstr ""

#: newmailnotifiersettingswidget.cpp:82
#, kde-format
msgid "Show Action Buttons"
msgstr ""

#: newmailnotifiersettingswidget.cpp:90
#, kde-format
msgid "Reply Mail"
msgstr ""

#: newmailnotifiersettingswidget.cpp:98 specialnotifierjob.cpp:154
#, kde-format
msgid "Reply to Author"
msgstr ""

#: newmailnotifiersettingswidget.cpp:98 specialnotifierjob.cpp:157
#, fuzzy, kde-format
#| msgid "&Select All"
msgid "Reply to All"
msgstr "&All utsöken"

#: newmailnotifiersettingswidget.cpp:107
#, kde-format
msgid "Display"
msgstr "Dorstellen"

#: newmailnotifiersettingswidget.cpp:112
#, kde-format
msgid "Enabled"
msgstr "Anmaakt"

#: newmailnotifiersettingswidget.cpp:116
#, kde-format
msgid "<a href=\"whatsthis\">How does this work?</a>"
msgstr "<a href=\"whatsthis\">Wodennig funkscheneert dat?</a>"

#: newmailnotifiersettingswidget.cpp:125
#, kde-format
msgid "Message:"
msgstr "Naricht:"

#: newmailnotifiersettingswidget.cpp:136
#, kde-format
msgid "Text to Speak"
msgstr "Text för't Vörlesen"

#: newmailnotifiersettingswidget.cpp:142
#, kde-format
msgid "Notify"
msgstr "Bescheed"

#: newmailnotifiersettingswidget.cpp:145
#, kde-format
msgid "Folders"
msgstr "Ornern"

#: newmailnotifiersettingswidget.cpp:148
#, kde-format
msgid "New Mail Notifier Agent"
msgstr "Nieg-Nettpost-Bescheedgeevhölper"

#: newmailnotifiersettingswidget.cpp:150
#, fuzzy, kde-format
#| msgid "Notifies about new mail."
msgid "Notify about new mails."
msgstr "Gifft över nieg Nettpost Bescheed."

#: newmailnotifiersettingswidget.cpp:152
#, fuzzy, kde-format
#| msgid "Copyright (C) 2013 Laurent Montel"
msgid "Copyright (C) 2013-%1 Laurent Montel"
msgstr "Copyright (C) 2013 Laurent Montel"

#: newmailnotifiersettingswidget.cpp:155
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: newmailnotifiersettingswidget.cpp:155
#, kde-format
msgid "Maintainer"
msgstr "Pleger"

#: newmailnotifiersettingswidget.cpp:156
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sönke Dibbern"

#: newmailnotifiersettingswidget.cpp:156
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "s_dibbern@web.de"

#: specialnotifierjob.cpp:116
#, kde-format
msgid "From: %1"
msgstr "Vun: %1"

#: specialnotifierjob.cpp:124
#, kde-format
msgid "Subject: %1"
msgstr "Bedraap; %1"

#: specialnotifierjob.cpp:127
#, kde-format
msgid "In: %1"
msgstr "Binnen: %1"

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Show mail..."
msgstr "Nettbreef wiesen..."

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Mark As Read"
msgstr ""

#: specialnotifierjob.cpp:150
#, kde-format
msgid "Delete"
msgstr ""

#~ msgid "New Mail Notifier settings"
#~ msgstr "Nieg-Nettpost-Bescheden instellen"

#~ msgid "Show button to display mail"
#~ msgstr "Knoop för't Wiesen vun den Nettbreef wiesen"

#~ msgid "Starting Jovie Text-to-Speech Service Failed %1"
#~ msgstr "Starten vun Vörleesdeenst \"Jovie\" fehlslaan: %1"
